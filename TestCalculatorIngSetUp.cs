using ClassCalculators;
using NUnit.Framework;

namespace ClassTests
{
	
	[TestFixture, Description("Use SetUp and TearDown")]
	public class TestCalculatorIngSetUp
	{
		ClassCalculators.CalculatorIng MyCalc;
		double a;
		[SetUp]
		public void Create()
		{
			MyCalc = new ClassCalculators.CalculatorIng();
		}

		[TearDown]
		public void Delete()

		{
			a = MyCalc.Multiplication(1, 0);
		}

		[Test]
		public void TestAdditionalIng()
		{
			Assert.AreEqual(2, MyCalc.Additional(1, 1), "7+0.1 should be equal to 7.1");
		}

		[Test]
		public void TestAdditionalIng2()
		{
			Assert.That(MyCalc.Additional(a, 5), Is.EqualTo(5));
		}
	}
}
