﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculators
{
	public class CalculatorIng : Calculator
	{
		public double Square(int num1)
		{
			double result = Math.Sqrt(num1);
			return result;
		}
		public double Degree(int num1, int num2)
		{
			double result = Math.Pow(num1, num2);
			return result;
		}
		public int MaxNumber(ref int[] NumberOfArray)
		{
			int result = NumberOfArray.Max();
			return result;
		}
		public int MinNumber(ref int[] NumberOfArray)
		{
			int result = NumberOfArray.Min();
			return result;
		}
		public double Percent(int num1, int num2)
		{
			int result = ((num1 * 100) / num2);
			return result;
		}
	}
}