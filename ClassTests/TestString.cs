﻿using NUnit.Framework;

namespace ClassTests
{
	[TestFixture, Description("Tests for String asserts")]
	public class TestString
	{
		[Test]
		public void TestStringContain()
		{
			string a = "Array should  be empty";
			StringAssert.Contains("Array should  be empty", a);
		}

		[Test]
		public void TestStringStart()
		{
			string a = "Array should  be empty";
			StringAssert.StartsWith("Array", a);
		}

		[Test]
		public void TestStringEnd()
		{
			string a = "Array should  be empty";
			StringAssert.EndsWith("be empty", a);
		}

		[Test]
		public void TestStringCase()
		{
			string a = "Array should  be empty";
			StringAssert.AreEqualIgnoringCase("aRRAY should  BE empty", a);
		}

		[Test]
		public void TestStringCaseThat()
		{
			string a = "Array should  be empty";
			Assert.That(a, Does.Contain("should"));
		}

		[Test]
		public void TestStringCaseThat1()
		{
			string a = "Array should be empty";
			Assert.That(a, Does.Not.Contain("shouldn't"));
		}

	}
}
