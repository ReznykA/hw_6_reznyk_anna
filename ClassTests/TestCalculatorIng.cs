﻿using ClassCalculators;
using NUnit.Framework;

namespace ClassTests
{
	[TestFixture, Description("Verify functions scientific calculator")]
	public class TestCalculatorIng
	{
		ClassCalculators.CalculatorIng MyCalc = new ClassCalculators.CalculatorIng();

		[Test, Order(5)]
		public void TestDegree()
		{
			Assert.AreEqual(8, MyCalc.Degree(2, 3), "Degree 2^3 should be equal to 8");
		}

		[Test, Ignore("the test is postponed to the next build")]
		public void TestDegreeIgnore()
		{
			Assert.True(-8 == MyCalc.Degree(-2, 3), "Degree 2^3 should be equal to -8");
		}

		[Test, Order(3)]
		public void TestAdditionalIng()
		{

			Assert.AreEqual(7.1, MyCalc.Additional(7, 0.1), "7+0.1 should be equal to 7.1");
		}

		[Test, Order(4)]
		public void TestSubstructionIng()
		{
			Assert.AreEqual(8, MyCalc.Substruction(6, -2), "6-(-2) should be equal to 8");
		}

		[Test, Order(1)]
		public void TestDivisionIng()
		{
			Assert.True(2.5 == MyCalc.Division(5, 2), "5/2 should be equal to 2.5");
		}

		[Test, Order(2)]
		public void TestMultiplicationIng()
		{
			Assert.Zero(MyCalc.Multiplication(7, 0), "7 *0 should be equal to 0");
		}

		[Test, Repeat(2)]
		public void TestMaxNumber()
		{
			int[] MaximumOfArray = new int[] { 1, 2, -3 };
			Assert.AreEqual(2, MyCalc.MaxNumber(MaximumOfArray), "max number should be equal to 2");
		}

		[Test, Retry(3)]
		public void TestMinNumber()
		{
			int[] MinimumOfArray = new int[] { 1, 2, -3 };
			Assert.AreEqual(-3, MyCalc.MinNumber(MinimumOfArray), "min number should be equal to -3");
		}

		[Test]
		public void TestPercentThat()
		{
			var result = MyCalc.Percent(3, 100);
			Assert.That(result, Is.EqualTo(3));
		}

		[Test(ExpectedResult = 4)]
		public int TestMultiplication2()
		{
			return 2 * 2; 
		}

		[Test, Description("Division to 0")]
		public void TestDivision2()
		{
			Assert.Zero(MyCalc.Division(0, 4), "0/4 should be equal to 0");
		}

		[TestCase(8, 16, 2)]
		[TestCase(-5, -10, 2)]
		[TestCase(-5, 10, -2)]
		[TestCase(3, -9, -3)]
		[TestCase(2.5, 5, 2)]
		public void Division(double expected, double num1, double num2)
		{
			Assert.AreEqual(expected, num1 / num2);
		}

		[Test]
		public void TestPass()
		{
			Assert.Pass("Test pass");
		}

		[Test]
		public void TestFail()
		{
			Assert.Fail("Test fail");
		}

		[Test]
		public void TestComparisonArray()
		{
			int[] MinimumOfArray = new int[] { 1, 2, -3 };
			int[] MaximumOfArray = new int[] { 1, 2, -3 };
			Assert.Greater(MyCalc.MaxNumber(MaximumOfArray), MyCalc.MinNumber(MinimumOfArray), "max number of first array should be greater than min number of second array");
		}

		[Test]
		public void TestComparisonArray1()
		{
			int[] MaximumOfArray = new int[] { 1, 2, -3 };
			int[] MinimumOfArray = new int[] { 5, 2, 4 };
			Assert.LessOrEqual(MyCalc.MaxNumber(MaximumOfArray), MyCalc.MinNumber(MinimumOfArray), "max number of first array should be less or equal than min number of second array");
		}

		[Test]
		public void TestNotEmptyArray()
		{
			int[] MaximumOfArray = new int[] { 5, 2, 4 };
			Assert.IsNotEmpty(MaximumOfArray, "Array should not be empty");
		}

		[Test]
		public void TestEmptyArray()
		{
			int[] MinimumOfArray = new int[] { };
			Assert.IsEmpty(MinimumOfArray, "Array should  be empty");
		}

		[Test]
		public void TestComparisonArrayThat()
		{
			int[] MaximumOfArray = new int[] { 1, 2, -3 };
			int[] MinimumOfArray = new int[] { 5, 2, 4 };
			Assert.That(MinimumOfArray, Is.Not.EquivalentTo(MaximumOfArray), "two arrays shouldn't be equialent");
		}

		[Test]
		public void TestComparisonArrayThat1()
		{
			int[] MaximumOfArray = new int[] { 1, 2, -3 };
			Assert.That(new int[] { 1, 2, -3 }, Is.EquivalentTo(MaximumOfArray), "two arrays should be equialent");
		}

		[Test]
		public void TestComparisonArrayThat2()
		{
			int[] MaximumOfArray = new int[] { 1, 7, 9 };
			Assert.That(MaximumOfArray, Is.All.InRange(0, 10), "Array should be in range 1 - 10");
		}

		[Test]
		public void TestDivisionIngThat()
		{
			Assert.That(MyCalc.Division(5, 2), Is.GreaterThan(2.0).And.LessThan(3.0));
		}
	}	
}
