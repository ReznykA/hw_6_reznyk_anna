﻿using ClassCalculators;
using NUnit.Framework;

namespace ClassTests
{

		[TestFixture]
		public class TestCalculator
		{
			Calculator MyCalc = new Calculator();

			[Test]
			public void TestAdditional()
			{
				Assert.AreEqual(2, MyCalc.Additional(-3, 5), "-3+5 should be equal to 2");
			}

			[Test]
			public void TestSubstruction()
			{
				Assert.AreEqual(-0.5, MyCalc.Substruction(4.5, 5), "4,5-5 should be equal to -0,5");
			}

			[Test]
			public void TestDivision()
			{
				Assert.AreEqual(3, MyCalc.Division(6, 2), "6/2 should be equal to 3");
			}

			[Test]
			public void TestDivisionNegativeDigits()
			{
				Assert.True(3 == MyCalc.Division(-6, -2), "-6/-2 should be equal to -3");
			}

			[Test]
			public void TestMultiplication()
			{
				Assert.AreEqual(12, MyCalc.Multiplication(3, 4), "3*4 should be equal to 12");
			}

			[Test]
			public void TestMultiplicationNegativeDigits()
			{
				Assert.IsTrue(12 == MyCalc.Multiplication(-3, -4), "3*4 should be equal to 12");
			}

			[Test(ExpectedResult = 4)]
			public int TestMultiplication2()
			{
				return 2 * 2; ;
			}

			[Test, Description("Division to 0")]
			public void TestDivision2()
			{
				Assert.Zero(MyCalc.Division(0, 4), "0/4 should be equal to 0");
			}

		}
	}
