﻿using System;
using System.Linq;


namespace ClassCalculators
{
	
	public class CalculatorIng : Calculator
	{
		public double SquareRoot(int num1)
		{
			double result = Math.Sqrt(num1);
			return result;
		}

		public double Degree(double operand, double degree)
		{
			double result = 1;

			for (; degree > 0; --degree)
			{
				result = Multiplication(operand, result);
			}
			return result;
		}

		public int MaxNumber(int[] MaximumOfArray)
		{
			int result = MaximumOfArray.Max();
			return result;
		}

		public int MinNumber(int[] MinimumOfArray)
		{
			int result = MinimumOfArray.Min();
			return result;
		}

		public double Percent(int num1, int num2)
		{
			double a = Multiplication(num1, 100);
			double result = Division(a, num2);
			return result;
		}
	}
}
